export const future = {
    webpack5: true,
    async headers() {
        return [
            {
                headers: [
                    { key: "Access-Control-Allow-Origin", value: "*" },
                    { key: "Access-Control-Allow-Headers", value: "*" },
                    { key: "Access-Control-Allow-Methods", value: "OPTIONS,POST,GET" }
                ]
            }
        ]
    }
}
