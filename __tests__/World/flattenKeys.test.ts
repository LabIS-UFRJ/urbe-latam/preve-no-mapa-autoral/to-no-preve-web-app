import { MapDataPotentiality } from "../../src/common/types/MapPoint"
import { flattenKeys } from "../../src/pages/api/helper"

describe("flattenKeys", () => {
    it("should return a flattened object", () => {
        expect(flattenKeys({
            value: {
                insideValue: {
                    insideInsideValue: ""
                }
            }
        })).toMatchObject(["value.insideValue.insideInsideValue"])
    })
})