import React from "react"
import styled from "styled-components"
import { Spin } from "antd"

export const SpinLoading = (): JSX.Element => {
    return <StyledSpinLoading />
}

const StyledSpinLoading = styled(Spin)`
    .ant-spin-dot {
        font-size: 80px;
    }
    .ant-spin-dot-item {
        background-color: orange;
        width: 30px;
        height: 30px;
    }
`
