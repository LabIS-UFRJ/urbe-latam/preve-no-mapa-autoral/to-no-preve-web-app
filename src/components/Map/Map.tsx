import React, { Fragment, ReactNode } from "react"
import { MapContainer, Marker, Tooltip, TileLayer } from "react-leaflet"
import { potentialityIcon, vulnerabilityIcon } from "./icons"

import { MapDataVulnerability, MapDataPotentiality } from "../../common/types/MapPoint"
import { LatLngTuple } from "leaflet"

import styled from "styled-components"

const CLASS_PREFIX = "MapContainer"

interface MapProps {
    potentiality?: MapDataPotentiality[]
    vulnerability?: MapDataVulnerability[]
    center: LatLngTuple
    tileServer: string
    dataEndpoint: string
    children?: ReactNode
}
const Map = ({
    potentiality,
    vulnerability,
    center,
    tileServer,
    dataEndpoint,
    children
}: MapProps): JSX.Element => {
    return (
        <StyledMapContainer center={center} zoom={15.5} scrollWheelZoom={true}>
            <TileLayer
                attribution='&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
                url={tileServer}
            />

            {potentiality?.map((point, index) => (
                <Fragment key={index}>
                    <Marker
                        position={[...point.posicao.coordinates].reverse() as LatLngTuple}
                        icon={potentialityIcon}
                    >
                        <Tooltip>
                            <div className={CLASS_PREFIX + "__Tooltip"}>
                                <img
                                    className={CLASS_PREFIX + "__Tooltip--Image"}
                                    src={point.image ? `${dataEndpoint}/media/${point.image}` : ""}
                                />
                                <span>
                                    <strong>Nome: </strong>
                                    {point.nome}
                                </span>
                                <span>
                                    <strong>Categoria: </strong>
                                    {point.tipoAreaDeUso.nome}
                                </span>
                            </div>
                        </Tooltip>
                    </Marker>
                </Fragment>
            ))}

            {vulnerability?.map((point, index) => (
                <Fragment key={index}>
                    <Marker
                        position={[...point.posicao.coordinates].reverse() as LatLngTuple}
                        icon={vulnerabilityIcon}
                    >
                        <Tooltip>
                            <div className={CLASS_PREFIX + "__Tooltip"}>
                                <img
                                    className={CLASS_PREFIX + "__Tooltip--Image"}
                                    src={point.image ? `${dataEndpoint}/media/${point.image}` : ""}
                                />
                                <span>
                                    <strong>Nome: </strong>
                                    {point.nome}
                                </span>
                                <span>
                                    <strong>Categoria: </strong>
                                    {point.tipoConflito.nome}
                                </span>
                            </div>
                        </Tooltip>
                    </Marker>
                </Fragment>
            ))}

            {children}
        </StyledMapContainer>
    )
}

const StyledMapContainer = styled(MapContainer)`
    height: 100vh;
    width: 100wh;
    .${CLASS_PREFIX} {
        &__Tooltip {
            display: flex;
            flex-direction: column;
            justify-content: center;
            align-items: center;
            &--Image {
                width: 500px;
                height: auto;
                margin-bottom: 8px;
            }
        }
    }
`

export default Map
