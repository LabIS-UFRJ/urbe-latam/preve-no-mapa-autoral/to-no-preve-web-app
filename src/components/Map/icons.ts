import { divIcon } from "leaflet"

export const potentialityIcon = divIcon({
    html: '<i style="color: green" class="far fa-map-marker-alt fa-2x"/>',
    iconSize: [32, 32],
    className: "conflictIcon"
})

export const vulnerabilityIcon = divIcon({
    html: '<i style="color: orange" class="fad fa-triangle fa-2x"/>',
    iconSize: [32, 32],
    className: "conflictIcon"
})
