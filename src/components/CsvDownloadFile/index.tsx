import React, { ReactElement } from "react"
import { CSVLink } from "react-csv"
import { format } from "date-fns"
import { MapDataPotentiality, MapDataVulnerability } from "../../common/types/MapPoint"
import { dataToCSV } from "../../pages/api/helper"
import { Censospreve } from "../../common/types/Censospreve"

interface FileProps {
    className: string
    potentiality?: MapDataPotentiality[]
    vulnerability?: MapDataVulnerability[]
    censosPreve?: Censospreve[]
}

const CSVDownloadButton = ({
    potentiality,
    vulnerability,
    className,
    censosPreve
}: FileProps): ReactElement => {
    const fileNamePotentiality = "potencialidade-" + format(new Date(), "dd-MM-yyyy") + ".csv"
    const fileNameVulnerability = "vulnerabilidade-" + format(new Date(), "dd-MM-yyyy") + ".csv"
    const fileNameCensosPreve = "censosPreve-" + format(new Date(), "dd-MM-yyyy") + ".csv"

    const parsedPotentiality = dataToCSV(potentiality as MapDataPotentiality[])
    const parsedVulnerability = dataToCSV(vulnerability as MapDataVulnerability[])
    const parsedCensosPreve = dataToCSV(censosPreve as Censospreve[])
    return (
        <>
            <span className={className} style={{ pointerEvents: "auto" }}>
                <CSVLink
                    data={parsedVulnerability || []}
                    separator={";"}
                    filename={fileNameVulnerability}
                >
                    Download Vulnerabilidades |
                </CSVLink>
                <CSVLink
                    data={parsedPotentiality || []}
                    separator={";"}
                    filename={fileNamePotentiality}
                >
                    | Download Potencialidade |
                </CSVLink>

                <CSVLink
                    data={parsedCensosPreve || []}
                    separator={";"}
                    filename={fileNameCensosPreve}
                >
                    Download Censo Prevê
                </CSVLink>
            </span>
        </>
    )
}
export default CSVDownloadButton
