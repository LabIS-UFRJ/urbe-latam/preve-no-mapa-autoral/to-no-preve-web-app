import React from "react"
import type { AppProps } from "next/app"
import { Global } from "../common/styles/globals"
import "antd/dist/antd.css"
import "leaflet/dist/leaflet.css"

const PreveNoMapa = ({ Component, pageProps }: AppProps): JSX.Element => {
    return (
        <>
            <Global />
            <Component {...pageProps} />
        </>
    )
}

export default PreveNoMapa
