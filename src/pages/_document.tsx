import Document, { Html, Head, Main, NextScript } from "next/document"

export default class MyDocument extends Document {
    render() {
        return (
            <Html lang="en">
                <Head>
                    {/* <!-- link manifest.json --> */}
                    <link rel="manifest" href="/manifest.json" />
                    {/* <!-- this sets the color of url bar  --> */}
                    <meta name="theme-color" content="#90cdf4" />
                    {/* <!-- this sets logo in Apple smatphones. --> */}
                    <link rel="apple-touch-icon" href="/ToNoMapaIcon_96x96.ico" />
                    {/* <!-- this sets the color of url bar in Apple smatphones --> */}
                    <meta name="apple-mobile-web-app-status-bar" content="#90cdf4" />

                    <link rel="shortcut icon" href="/favicon.png" type="image/png" />
                    <link
                        rel="stylesheet"
                        href="https://pro.fontawesome.com/releases/v5.10.0/css/all.css"
                        integrity="sha384-AYmEC3Yw5cVb3ZcuHtOA93w35dYTsvhLPVnYs9eStHfGJvOvKxVfELGroGkvsg+p"
                        crossOrigin="anonymous"
                    />
                </Head>
                <body>
                    <Main />
                    <NextScript />
                </body>
            </Html>
        )
    }
}
