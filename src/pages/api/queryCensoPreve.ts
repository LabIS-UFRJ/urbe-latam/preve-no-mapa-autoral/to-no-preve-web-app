import { gql } from "@apollo/client"
import { Censospreve } from "../../common/types/Censospreve"



export interface CensospreveData {
  censospreve: Censospreve[]
}

export const GET_CENSOS_PREVE = gql`
    query { 
        censospreve {
                id
                posicao
                image
                entrevistador
                setor
                endereco
                telefone
                numeroCasa
                numMen18
                num1855
                num55Mais
                numNe
                numPets
                matPredom
                sustentacao
                conserv
                matTelhado
                matCobre
                idadeCasa
                ultRef
                numPisos
                pisoMorador
                matAndMora
                evento {
                  id
                  tipo
                  data
                  impacto
                  reparo
                }
                imgId
              }
      }
    
`