interface World {
    first: string
    second: string
}
export const RequestWorld = ({ first, second }: World): string => {
    return first + second
}
