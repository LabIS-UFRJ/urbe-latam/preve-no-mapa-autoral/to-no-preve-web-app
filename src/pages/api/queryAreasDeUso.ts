import { gql } from "@apollo/client"
import { MapDataVulnerability, MapDataPotentiality } from "../../common/types/MapPoint"

export interface AreasDeUsoData {
    areasDeUso: MapDataPotentiality[]
}

export interface AreaDeConflitoData {
    conflitos: MapDataVulnerability[]
}

export const GET_AREAS_DE_USO = gql`
    query {
        areasDeUso {
            id
            nome
            posicao
            image
            tipoAreaDeUso {
                id
                nome
            }
        }
    }
`

export const GET_AREAS_DE_CONFLITO = gql`
    query {
        conflitos {
            id
            nome
            posicao
            image
            tipoConflito {
                id
                nome
            }
        }
    }
`
