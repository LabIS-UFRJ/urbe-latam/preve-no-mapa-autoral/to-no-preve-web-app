export const sleep = async (time: number) => {
    return new Promise((resolve) => setTimeout(resolve, time))
}

export const flattenKeys = (
    data: object
) => {
    let finalKeys = [] as string[]
    if (data === null) {
        return finalKeys
    }

    Object.entries(data).map(([key, value], _index) => {
        if (typeof value === "object") {
            const insideKeys = flattenKeys(value).map((res) => key + "." + res)
            finalKeys = [...finalKeys, ...insideKeys]
        } else {
            finalKeys.push(`${key}`)
        }
    })

    return [...finalKeys]
}

export const flattenValues = (
    data: object
) => {
    let finalValues = [] as string[]
    if (!data) {
        return finalValues
    }

    Object.entries(data).map(([_key, value], _index) => {
        if (typeof value === "object") {
            const insideValues = flattenValues(value)
            finalValues = [...finalValues, ...insideValues]
        } else {
            finalValues.push(`${value}`)
        }
    })

    return [...finalValues]
}

export const dataToCSV = (
    data: object[]
): string[][] | null => {
    let keys = [] as string[]
    if (!data) {
        return [[], []]
    }

    data.forEach((point) => {
        let currentKeys = flattenKeys(point)
        if (currentKeys.length > keys.length) {
            keys = currentKeys
        }
    })

    let values = [] as string[][]
    data.forEach((point) => {
        values.push(flattenValues(point))
    })

    return [[...keys], ...values] as string[][]
}
