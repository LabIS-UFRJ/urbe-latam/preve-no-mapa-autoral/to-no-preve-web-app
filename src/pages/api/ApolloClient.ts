import { ApolloClient, createHttpLink, InMemoryCache } from "@apollo/client"
import { setContext } from "@apollo/client/link/context"

const httpLink = createHttpLink({
    uri: `${process.env.NEXT_PUBLIC_DASHBOARD_ENDPOINT}/graphql`
})

const context = setContext((_, { headers }) => {
    return {
        headers: {
            ...headers
        },
        fetchOptions: {
            mode: "cors",
            method: "POST"
        }
    }
})

export const client = new ApolloClient({
    link: context.concat(httpLink),
    cache: new InMemoryCache()
})
