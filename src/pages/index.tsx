import React, { CSSProperties, useEffect, useState } from "react"
import { LatLngTuple } from "leaflet"
import { useQuery } from "@apollo/react-hooks"
import { client } from "./api/ApolloClient"

import {
    GET_AREAS_DE_USO,
    AreasDeUsoData,
    GET_AREAS_DE_CONFLITO,
    AreaDeConflitoData
} from "./api/queryAreasDeUso"

export const DASHBOARD_ENDPOINT = process.env.NEXT_PUBLIC_DASHBOARD_ENDPOINT as string
export const TILE_SERVER = process.env.NEXT_PUBLIC_TILE_SERVER as string

import dynamic from "next/dynamic"
const Map = dynamic(() => import("../components/Map/Map"), { ssr: false })
import { SpinLoading } from "../components/SpinLoading/SpinLoading"
import CSVDownloadButton from "../components/CsvDownloadFile"
import { CensospreveData, GET_CENSOS_PREVE } from "./api/queryCensoPreve"

const Home = (): JSX.Element => {
    const [dataAPILoading, setDataAPILoading] = useState<boolean>(false)
    const {
        loading: areasDeUsoLoading,
        error: areasDeUsoError,
        data: AreasDeUsoData
    } = useQuery<AreasDeUsoData>(GET_AREAS_DE_USO, { client })

    const {
        loading: areasConflitoLoading,
        error: areasConflitoError,
        data: AreaDeConflitoData
    } = useQuery<AreaDeConflitoData>(GET_AREAS_DE_CONFLITO, { client })

    const {
        loading: censosPreveLoading,
        error: censosPreveError,
        data: censosPreveData
    } = useQuery<CensospreveData>(GET_CENSOS_PREVE, { client })

    if (areasDeUsoError || areasConflitoError) {
        console.log(areasDeUsoError, areasConflitoError, censosPreveError)
    }

    const dataIsBeingLoaded = (): boolean => {
        if (!areasConflitoLoading && !areasDeUsoLoading && !censosPreveLoading) {
            return false
        }

        return true
    }

    useEffect(() => {
        if (dataIsBeingLoaded()) {
            setDataAPILoading(true)
        } else {
            setDataAPILoading(false)
        }
    }, [areasConflitoLoading, areasConflitoLoading, censosPreveData])

    return (
        <>
            <div
                className={"LoadingContainer"}
                style={{
                    ...LoadingContainerStyles,
                    display: dataAPILoading ? "flex" : "none"
                }}
            >
                <SpinLoading />
            </div>
            {!dataAPILoading && (
                <Map
                    potentiality={AreasDeUsoData?.areasDeUso}
                    vulnerability={AreaDeConflitoData?.conflitos}
                    center={[-22.93567, -43.09935] as LatLngTuple}
                    tileServer={TILE_SERVER}
                    dataEndpoint={DASHBOARD_ENDPOINT}
                >
                    <CSVDownloadButton
                        potentiality={AreasDeUsoData?.areasDeUso}
                        vulnerability={AreaDeConflitoData?.conflitos}
                        censosPreve={censosPreveData?.censospreve}
                        className={"leaflet-bottom"}
                    />
                </Map>
            )}
        </>
    )
}

const LoadingContainerStyles: CSSProperties = {
    display: "flex",
    alignContent: "center",
    justifyContent: "center",
    alignItems: "center",
    visibility: "visible",
    width: "auto",
    height: "100vh",
    backgroundColor: "white",
    transition: "s",
    opacity: "1"
}

export default Home
