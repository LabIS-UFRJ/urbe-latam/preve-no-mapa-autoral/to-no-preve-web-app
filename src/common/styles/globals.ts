import { createGlobalStyle } from "styled-components"

export const Global = createGlobalStyle`
    /* On Chrome */
    .hide-scrollbar::-webkit-scrollbar {
        display: none;
    }
    /* For Firefox and IE */
    .hide-scrollbar {
        scrollbar-width: none;
        -ms-overflow-style: none;
    }
    
    body {
        display: block;
        margin: 0px;
    }

    button {
        cursor: pointer;
        outline: none;
    }
`
