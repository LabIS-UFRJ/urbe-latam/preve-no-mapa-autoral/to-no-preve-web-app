import { LatLngTuple } from "leaflet"

export interface MapAreaTypeBase {
    id: number
    nome: string // buteco, mercado..
}

export interface MapPointTypeBase {
    id: number
    nome: string // bar do palmeira, ..
    image: string
    posicao: {
        type: string
        coordinates: LatLngTuple
    }
}

export interface MapDataPotentiality extends MapPointTypeBase {
    tipoAreaDeUso: MapAreaTypeBase
}

export interface MapDataVulnerability extends MapPointTypeBase {
    tipoConflito: MapAreaTypeBase
}
