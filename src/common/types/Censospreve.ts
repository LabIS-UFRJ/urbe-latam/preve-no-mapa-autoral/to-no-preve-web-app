import { LatLngTuple } from "leaflet"
export interface Censospreve {
    id: string | null
    posicao: LatLngTuple
    image: string
    entrevistador: string
    setor: number
    endereco: string
    telefone: string
    numeroCasa: string
    numMen18: number
    num1855: number
    num55Mais: number
    numNe: number
    numPets: number
    matPredom: string
    sustentacao: string
    conserv: string
    matTelhado: string
    matCobre: string
    idadeCasa: number
    ultRef: string
    numPisos: number
    pisoMorador: string
    matAndMora: string,
    evento:  {
        id: number
        tipo: string
        data: Date
        impacto: string
        reparo: string
    } | null
    imgId: string
}
